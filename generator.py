# PSZT - Nawigator
# Generator losowych grafów
# Marek Cagara
# Pawel Kubik
# Patryk Szypulski

import graph
import itertools
import math
import random
from lib2to3.pgen2.tokenize import Exponent

# n: liczba wierzcholkow
def generate_vertices(n):
    result = []

# generowanie na brzegach kwadratu
#     verticesPerSide = [0, 0, 0, 0]
#     for i in range(n):
#         verticesPerSide[random.randint(0,3)] += 1
#      
#     if verticesPerSide[0] is not 0:
#         vertexSpace = 1/verticesPerSide[0]
#     for i in range(verticesPerSide[0]):
#         result.append( (0.0, random.uniform(i*vertexSpace,(i+1)*vertexSpace)) )
#          
#     if verticesPerSide[1] is not 0:
#         vertexSpace = 1/verticesPerSide[1]
#     for i in range(verticesPerSide[1]):
#         result.append( (1.0, random.uniform(i*vertexSpace,(i+1)*vertexSpace)) )
#          
#     if verticesPerSide[2] is not 0:
#         vertexSpace = 1/verticesPerSide[2]
#     for i in range(verticesPerSide[2]):
#         result.append( (random.uniform(i*vertexSpace,(i+1)*vertexSpace), 0.0) )
#      
#     if verticesPerSide[3] is not 0:
#         vertexSpace = 1/verticesPerSide[3]    
#     for i in range(verticesPerSide[3]):
#         result.append( (random.uniform(i*vertexSpace,(i+1)*vertexSpace), 1.0) )

#generowanie na brzegach i wewnątrz    
    for i in range(n):
        result.append( (random.uniform(0.0,1.0), random.uniform(0.0,1.0)) )

    return result    

# r: parametr funkcji prawdopodobienstwa
# d: odleglosc miedzy punktami
def g(r, d):
    return math.exp(-d/r)

# V: lista wierzcholkow [(float,float)]
# r: parametr funkcji prawdopodobienstwa istnienia krawedzi
def generate_edges(V, r):
    E = [[] for i in range(len(V))]
    for (i,u),(j,v) in itertools.combinations(enumerate(V),2):
        d = math.hypot(u[0]-v[0], u[1]-v[1])
        x = random.uniform(0.0, 1.0)
        if x < g(r, d):
            E[i].append(j)
            E[j].append(i)
    return E
