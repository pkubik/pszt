# PSZT - Nawigator
# Graf
# Marek Cagara
# Pawel Kubik
# Patryk Szypulski

import generator as gen
from heapq import *
import math

# Niezalezna klasa reprezentujaca graf
class Graph:
    def __init__(self, n, r):
        """
        Generuje losowy graf o zadanych parametrach.
        n -- liczba wierzcholkow
        r -- parametr funkcji prawdopodobienstwa instnienia krawedzi
        """
        self.V = gen.generate_vertices(n)
        self.E = gen.generate_edges(self.V, r)

    def d(self, u, v):
        u = self.V[u]
        v = self.V[v]
        return math.hypot(u[0]-v[0], u[1]-v[1])

# Klasy specyfikujące problem poszukiwania najkrotszej sciezki
class PathFindingState:
    def __init__(self, problem, v, prev, cost):
        self.problem = problem
        self.v = v
        self.prev = prev
        self.cost = cost

    def actions(self):
        """
        Generator dostepnych przejsc dla danego stanu
        """
        self.problem.closed[self.v] = True
        for adj in self.problem.G.E[self.v]:
            if not self.problem.closed[adj]:
                cost = self.cost + self.problem.G.d(self.v, adj)
                yield PathFindingState(self.problem, adj, self, cost)


class PathFindingProblem:
    def __init__(self, graph, u, v):
        self.G = graph
        self.finish = v
        self.start = PathFindingState(self, u, None, 0.0)
        self.closed = [False] * len(self.G.V)

    def terminal(self, state):
        return state.v == self.finish

    def heuristic(self, state):
        return self.G.d(state.v, self.finish)
    
    def path_to(self, state):
        path = []
        while state is not None:
            path.insert(0, state.v)
            state = state.prev
        return path


