#! /usr/bin/python3

# PSZT - Nawigator
# GUI
# Marek Cagara
# Pawel Kubik
# Patryk Szypulski

import gui
import sys

def main():
    n = 100
    r = 0.1
    if len(sys.argv) >= 2:
        n = int(sys.argv[1])
        r = float(sys.argv[2])
    gui.launch(n, r)

if __name__ == "__main__":
    main()

