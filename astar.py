# PSZT - Nawigator
# Graf
# Marek Cagara
# Pawel Kubik
# Patryk Szypulski

from heapq import *

def solve(problem):
    """
    Pobiera obiekt klasy specyfikujacej problem ktory ma byc rozwiazany. Musi ona zawierac:
    start -- stan poczatkowy
    terminal -- funkcja okreslajaca czy dany stan jest terminalny
    heuristic -- funkcja obliczajaca heurystyke
    
    Dodatkowo stan obiektu musi posiadac metode actions zwracajaca wszystkie stany,
    ktore mozna z niego osiagnac.

    Rozwiazaniem problemu jest stan terminalny.
    """
    q = [(0.0, problem.start)]
    while len(q) > 0:
        _,current = heappop(q)
        if problem.terminal(current):
            return current
        else:
            for s in current.actions():
                heappush(q, (problem.heuristic(s) + s.cost, s))
    return None
