#! /usr/bin/python3

# PSZT - Nawigator
# GUI
# Marek Cagara
# Pawel Kubik
# Patryk Szypulski
import sys
from tkinter import *

import graph
import astar
import math

class Vertex(object):
    """
    Klasa reprezentująca jeden wierzchołek w narysowanym grafie.
    Potrafi rysować się oraz obsługiwać zdarzenia kliknięcia
    """
    DEFAULT_TEXT_COLOR = "#000"
    DEFAULT_OVAL_COLOR = "#0f0"
    
    _curr_max = -1
    
    def __init__(self, canvas, position=(0,0), oval_color = None, text_color = None, size = 50, name = None):
        self.canvas = canvas
        self.position = position
        self.size = size
        if oval_color is None:
            self.oval_color = Vertex.DEFAULT_OVAL_COLOR
        else:
            self.oval_color = oval_color
        if text_color is None:
            self.text_color = Vertex.DEFAULT_TEXT_COLOR
        else:
            self.oval_color = oval_color
        self.oval_id = -1
        self.text_id = -1
        Vertex._curr_max += 1
        self.number = Vertex._curr_max
        self.tag = "vert_%d" % self.number
        
        if name is not None:
            self.name = name
        else:
            self.name = "Vertex_%d" % self.number
    
    def draw(self):
        self.oval_id = self.canvas.create_oval(self.position[0]-self.size/2, self.position[1]-self.size/2, self.position[0]+self.size/2, self.position[1]+self.size/2, fill=self.oval_color, tag=self.tag)
        self.text_id = self.canvas.create_text(self.position[0], self.position[1], text=self.number, font=("Purisa", int(self.size/3)), tag=self.tag, fill=self.text_color)
        
    def changeColor(self, oval_color=None, text_color=None):
        
        if oval_color is not None:
            self.canvas.itemconfigure(self.oval_id, fill=oval_color)
        else:
            self.canvas.itemconfigure(self.oval_id, fill=Vertex.DEFAULT_OVAL_COLOR)
        if text_color is not None:
            self.canvas.itemconfigure(self.text_id, fill=text_color)
        else:
            self.canvas.itemconfigure(self.text_id, fill=Vertex.DEFAULT_TEXT_COLOR)
        
    def bindLeftClick(self, function):
        self.canvas.tag_bind(self.tag, "<Button-1>", function)
    
    def bindRightClick(self, function):
        self.canvas.tag_bind(self.tag, "<Button-3>", function)
        
    def __str__(self):
        return self.name

class Edge(object):
    """
    Klasa reprezentująca krawędź w rysowanym grafie.
    """
    DEFAULT_EDGE_COLOR = "#000"
    DEFAULT_EDGE_WIDTH = 2
    SELECTED_EDGE_COLOR = "#7D4461"
    SELECTED_EDGE_WIDTH = 4
    
    def __init__(self, canvas, start_vertex, end_vertex, edge_color=None):
        self.start = start_vertex
        self.end = end_vertex
        self.tag = "edge_%d_%d" % (self.start.number, self.end.number)
        self.canvas = canvas
        self.edge_id = -1
        self._selected = False
        
        if edge_color is None:
            self.color = Edge.DEFAULT_EDGE_COLOR
        else:
            self.color = edge_color
    
    def draw(self):
        delta_x = self.end.position[0] - self.start.position[0]
        delta_y = self.end.position[1] - self.start.position[1]
        arc_tan = math.atan2(delta_y, delta_x)
         
        start_x = self.start.position[0] + self.start.size*math.cos(arc_tan)/2
        start_y = self.start.position[1] + self.start.size*math.sin(arc_tan)/2
        end_x = self.end.position[0] - self.end.size*math.cos(arc_tan)/2
        end_y = self.end.position[1] - self.end.size*math.sin(arc_tan)/2
             
        self.edge_id = self.canvas.create_line(start_x, start_y, end_x, end_y, fill=self.DEFAULT_EDGE_COLOR, width=Edge.DEFAULT_EDGE_WIDTH, tag = self.tag )
    
    def setSelected(self, selected):
        self._selected = selected
        
        if selected is True:
            self.canvas.itemconfig(self.edge_id, fill=Edge.SELECTED_EDGE_COLOR, width=Edge.SELECTED_EDGE_WIDTH)
        elif selected is False:
            self.canvas.itemconfig(self.edge_id, fill=Edge.DEFAULT_EDGE_COLOR, width=Edge.DEFAULT_EDGE_WIDTH)    
            
    def toggleSelection(self):
        if self._selected is False:
            self.setSelected(True)
        else:
            self.setSelected(False)
            
    def __str__(self):
        return "Krawędź %d -> %d" % (self.start.number, self.end.number)
   
class Application(object):
    """
    Klasa reprezentująca okno programu.
    """
    
    START_COLOR = "#f00"
    END_COLOR = "#00f"
    VERTEX_SIZE = 30
    
    def __init__(self, title, n, r, width=500, height=768):
        self.root = Tk()
        self.frame = Frame(self.root)
        self.root.title(title)
        
        self.width = width
        self.height = height
        self.canvas = Canvas(self.frame, width=width, height=height)
        if self.width < self.height:
            self.graph_size = self.width
        else:
            self.graph_size = self.height
            
        self.selected_start = None
        self.selected_end = None
        self.selected_edges = []
        
        self.frame.pack()
        self.loadGraph(n, r)
        self.drawGraph()
        self.canvas.pack()
    
    def run(self):
        self.frame.mainloop()   
        
        
    def loadGraph(self, n, r):
        """
        Generuje graf o zadanych parametrach i buduje odpowiednie struktury,
        niezbędne do prawidłowego wyświetlania
        """
        
        self.graph = graph.Graph(n, r)

        #tworzenie obiektów wierzchołków i dodawanie tagów do pomocniczego słownika 
        self.vertices = {}
        self.tag_num_dict = {}
        for vertex in self.graph.V:
            vertex_object = Vertex(canvas=self.canvas, position=((0.1 + vertex[0]*0.8)*self.graph_size, (0.1+vertex[1]*0.8)*self.graph_size), size=Application.VERTEX_SIZE)
            self.vertices[vertex_object.number] = vertex_object
            self.tag_num_dict[vertex_object.tag] = vertex_object.number
        
        
        self.edges = {}
        for num, vertices in enumerate(self.graph.E):
            # przetwarzanie i tworzenie zbioru krawędzi w formacie dwuwymiarowego słownika 
            for vertex in vertices:
                if vertex > num:
                    if num not in self.edges:
                        self.edges[num] = {}
                    self.edges[num][vertex] = Edge(self.canvas, self.vertices[num],self.vertices[vertex])
                    
    def drawGraph(self):
        """
        Rysuje graf w oknie programu.
        """
        if not self.graph:
            return
        
        #rysowanie wierzchołków i dopinanie funkcji obsługujących zdarzenia
        for vertex in self.vertices.values():
            vertex.draw()
            vertex.bindLeftClick(self._startClick)
            vertex.bindRightClick(self._endClick)
            
        
        #rysowanie obiektów krawędzi
        for first_vert in self.edges.values():
            for edge in first_vert.values():
                edge.draw();
    
    
    def _startClick(self, event):
        """
        Funkcja obsługująca kliknięcia wskazujące punkt początkowy poszukiwanej trasy.
        """
        self.resetPath()
        
        itemId,  = self.canvas.find_closest(event.x, event.y)
        tag = self.canvas.gettags(itemId)[0]
        
        # wskazanie startu w miejscu już wskazanego stopu
        if self.selected_end == self.tag_num_dict[tag]:
            self.vertices[self.selected_end].changeColor()
            self.selected_end = None;
            
            if self.selected_start is not None:
                self.vertices[self.selected_start].changeColor()
            self.selected_start = self.tag_num_dict[tag];
            self.vertices[self.selected_start].changeColor(Application.START_COLOR)
        
        # wskazanie startu gdy nie ma innego startu wskazanego
        elif self.selected_start == None:
            self.vertices[self.tag_num_dict[tag]].changeColor(Application.START_COLOR)
            self.selected_start = self.tag_num_dict[tag]
        
        # wskazanie startu w misjcu, gdzie już był start wskazany   
        elif self.selected_start == self.tag_num_dict[tag]:
            self.vertices[self.tag_num_dict[tag]].changeColor()
            self.selected_start = None
        
        # wskazanie startu w punkcie niewskazsanym przez start i stop gdy nie ma określonego startu
        else:
            self.vertices[self.selected_start].changeColor()
            self.selected_start = self.tag_num_dict[tag]
            self.vertices[self.selected_start].changeColor(Application.START_COLOR)
        
        # jeśli zaznaczony start i stop to wyznacz ścieżkę
        if (self.selected_start != None) and (self.selected_end != None):
            self._generatePath()
        
    def _endClick(self, event):
        """
        Funkcja obsługująca kliknięcia wskazujące punkt końcowy poszukiwanej trasy.
        """
        
        self.resetPath()
        
        itemId,  = self.canvas.find_closest(event.x, event.y)
        tag = self.canvas.gettags(itemId)[0]
        
        # wskazanie stopu w miejscu już wskazanego startu
        if self.selected_start == self.tag_num_dict[tag]:
            self.vertices[self.selected_start].changeColor()
            self.selected_start = None;
            
            if self.selected_end is not None:     
                self.vertices[self.selected_end].changeColor()
            self.selected_end = self.tag_num_dict[tag];
            self.vertices[self.selected_end].changeColor(Application.END_COLOR)
            
        # wskazanie stopu gdy nie ma innego stopu wskazanego
        elif self.selected_end == None:
            self.vertices[self.tag_num_dict[tag]].changeColor(Application.END_COLOR)
            self.selected_end = self.tag_num_dict[tag]
        
        # wskazanie stopu w misjcu, gdzie już był stop wskazany    
        elif self.selected_end == self.tag_num_dict[tag]:
            self.vertices[self.tag_num_dict[tag]].changeColor()
            self.selected_end = None
        # wskazanie stopu w punkcie niewskazsanym przez start i stop gdy nie ma określonego stopu    
        else:
            self.vertices[self.selected_end].changeColor()
            self.selected_end = self.tag_num_dict[tag]
            self.vertices[self.selected_end].changeColor(Application.END_COLOR)
        
        # jeśli zaznaczony start i stop to wyznacz ścieżkę
        if (self.selected_start != None) and (self.selected_end != None):
            self._generatePath()
    
    def resetPath(self):
        """
        Resetuje zaznaczone krawędzie, jeśli jakieś istnieją.
        """
        for selected_edge in self.selected_edges:
            selected_edge.setSelected(False)
        self.selected_edges = []
        
    def _generatePath(self):
        """
        Generuje najkrótszą ścieżkę i ją rysuje
        """
        
        problem = graph.PathFindingProblem(self.graph, self.selected_start, self.selected_end)
        self.path = problem.path_to(astar.solve(problem))
        
        if len(self.path) > 0:
            str = "Znaleziona ścieżka: %d " % self.path[0]
            for i in range(0,len(self.path)-1):
                str += "-> %d" % self.path[i+1]
                if self.path[i] < self.path[i+1]:
                    selected_edge = self.edges[self.path[i]][self.path[i+1]]
                else:
                    selected_edge = self.edges[self.path[i+1]][self.path[i]]
                
                selected_edge.setSelected(True)
                self.selected_edges.append(selected_edge)
            
            print(str) 
        else:
            print("Brak ścieżki")

def launch(n, r):
    app = Application("Nawigator", n, r, width=1000, height=1000)
    app.run()

def main():
    n = 100
    r = 0.1
    if len(sys.argv) >= 2:
        n = int(sys.argv[1])
        r = float(sys.argv[2])
    launch(n, r)

if __name__ == "__main__":
    main()

